package com.zuitt.discussion.controllers;

import com.zuitt.discussion.exceptions.UserException;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userServices;

    @RequestMapping(value ="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody User user){
        userServices.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }
    @RequestMapping(value ="/users", method = RequestMethod.GET)
    public ArrayList<User> getallUser(){

        return userServices.getallUsers();
    }
    @RequestMapping(value ="/getusers", method = RequestMethod.GET)
    public Optional<User> getUser(@RequestParam Long id){

        return userServices.getUser(id);
    }
    @RequestMapping(value ="/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Long id){

        return userServices.deleteUser(id);
    }
    @RequestMapping(value ="/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateUser(@PathVariable Long id,@RequestBody User updated){
        return userServices.updateUser(id, updated);
    }
    @RequestMapping(value="/users/register", method = RequestMethod.POST)
    // Register method takes a" request body as a Map of key-value pairs", where the keys are strings and the values are strings. It also throws a "UserException" in case of an error, which is a "custom exception".
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        // This retrieves the value associated with the "username" key from the request body "Map" and assigns it to a String variable called "username".
        String username = body.get("username");

        // check if the user provided "username" exists in the database, if the user exists, it throws a UserException with the message "Username already exists."
        if(!userServices.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        }
        // if username doesn't exists, it will proceed on creating of the client.
        else {
            // This retrieves the value associated with the "password" key from the request body "Map" and assigns it to a String variable called "password".
            String password = body.get("password");

            //This encrypts the password using the BCryptPasswordEncode, and store it to the "encodedPassword" variable.
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            // Instantiates the User model to create a new user
            User newUser = new User(username, encodedPassword);

            // saves in the "newUser" in the database.
            userServices.createUser(newUser);

            // Sends a "User registered successfully" message as the response body and an HTTP status code of 201.
            return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
        }


    }


}
